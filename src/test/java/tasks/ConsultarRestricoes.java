package tasks;

import datamass.inputCpf;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Test;
import setup.Setup;

public class ConsultarRestricoes extends Setup {


    @Test
    public void  validarCPFcomRestircoes(){

    RestAssured.given()
            .when()
            .get("/restricoes/"+inputCpf.restricao)
            .then()
            .log().all()
                .statusCode(200)
            .body("mensagem",Matchers.is("O CPF " +inputCpf.restricao+ " tem problema"))
            ;

    }

    @Test
    public void  validarCPFsemRestricoes(){

        RestAssured.given()
                .when()
                .get("/restricoes/"+inputCpf.valido)

                .then()
                .log().all()
                .statusCode(204)
        ;

    }
}
