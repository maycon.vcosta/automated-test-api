package tasks;

import datamass.Usuarios.Davi;

import org.hamcrest.Matchers;
import org.junit.Test;
import setup.Setup;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;

public class CriarSimulacoes extends Setup {

    Map<Object, Object> params = new HashMap<>();
    Davi user = new Davi();


    @Test
    public void criarSimulacaoComSucesso() {

        params.put("nome",Davi.nome);
        params.put("cpf",Davi.cpf);
        params.put("email", Davi.email);
        params.put("valor", Davi.valor);
        params.put("parcelas", Davi.parcelas);
        params.put("seguro", Davi.seguro);


       given()
                .contentType("application/json")
                .body(params)
                .when()
                .post("/simulacoes")
                .then()
                .statusCode(201)
                .body("parcelas", Matchers.is(Davi.parcelas))
                .body("seguro", Matchers.is(Davi.seguro))
                .body("cpf", Matchers.is(Davi.cpf))
                .body("valor", Matchers.is(Davi.valor))
                .body("nome", Matchers.is(Davi.nome))
                .body("email", Matchers.is(Davi.email))
        ;
    }

    @Test
    public void ValidarErroCampoCpfNulo(){

        params.put("nome", Davi.nome);
        params.put("cpf", "");
        params.put("email", Davi.email);
        params.put("valor", Davi.valor);
        params.put("parcelas", Davi.parcelas);
        params.put("seguro", Davi.parcelas);

        given()
                .contentType("application/json")
                .body(params)
                .when()
                .post("/simulacoes")

                .then()
                .statusCode(400);

    }
    @Test
    public void ValidarCamposnulos(){

        params.put("nome", "");
        params.put("cpf", "");
        params.put("email", "");
        params.put("valor", "");
        params.put("parcelas", "");
        params.put("seguro", "");

        given()
                .contentType("application/json")
                .body(params)
                .when()
                .post("/simulacoes")

                .then()
                .statusCode(400);
    }

    @Test
    public void ValidarRegraValor() {

        params.put("nome", Davi.nome);
        params.put("cpf", Davi.cpf);
        params.put("email", Davi.email);
        params.put("valor", 50000);
        params.put("parcelas", Davi.parcelas);
        params.put("seguro", Davi.seguro);

        given()
                .contentType("application/json")
                .body(params)
                .when()
                .post("/simulacoes")

                .then()
                .statusCode(400);
    }

}


