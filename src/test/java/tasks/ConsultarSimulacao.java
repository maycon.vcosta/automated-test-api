package tasks;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Test;
import setup.Setup;
import utils.GeradorCPF;
import utils.GerarDados;


public class ConsultarSimulacao extends Setup {

    @Test
    public void consultaCpfNaoCadastrado() {

        GeradorCPF gerar = new GeradorCPF();
        String cpfGerado = gerar.geraCPF();


        RestAssured.given()
                .when()
                .get("/simulacoes/"+ cpfGerado)
                .then()
                .statusCode(404)
                .body("mensagem", Matchers.is("CPF "+cpfGerado+" não encontrado"));

    }

    @Test
    public void consultaCpfcadastrado() {

        GerarDados gerar = new GerarDados();
        String cpfcadastrado = gerar.cpfCadastrado();

        RestAssured.given()
                .when()
                .get("/simulacoes/"+cpfcadastrado)
                .then()
                .statusCode(200)
                .log().all()
                .body(Matchers.is(Matchers.notNullValue()))
               ;

    }

}
