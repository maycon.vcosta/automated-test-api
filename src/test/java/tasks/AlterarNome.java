package tasks;


import datamass.Usuarios.Andre;
import datamass.Usuarios.Davi;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Test;
import setup.Setup;
import utils.GeradorCPF;
import utils.GerarDados;


import java.util.HashMap;
import java.util.Map;

public class AlterarNome extends Setup {

    @Test
    public void alterarNomeComSucesso(){
        Map<Object, Object> params = new HashMap<>();

        GerarDados gerar = new GerarDados();
        gerar.massaAlterarSimulacao();


        params.put("nome","nome alterado");
        params.put("cpf", Andre.cpf);
        params.put("email",Andre.email);
        params.put("valor",Andre.valor);
        params.put("parcelas",Andre.parcelas);
        params.put("seguro",Andre.seguro);

        RestAssured.given()
                .contentType("application/json")
                .body(params)
                .when()
                .put("/simulacoes/"+Andre.cpf)
                .then()
                .statusCode(200)
                .body("id", Matchers.is(Matchers.notNullValue()))
                .body("nome", Matchers.is("nome alterado"))

        ;}


    }



