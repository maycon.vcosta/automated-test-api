package tasks;

import io.restassured.RestAssured;
import org.junit.Test;
import setup.Setup;
import utils.GerarDados;

public class DeletarSimulacao extends Setup {

    @Test
    public void deletarSimulacoesComSucesso() {

        GerarDados gerar = new GerarDados();
        Number id = gerar.massadelete();

        RestAssured.given()
                .when()
                .delete("/simulacoes/"+ id)
                .then()
                .statusCode(200);

    }
}
