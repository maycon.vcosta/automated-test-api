package tasks;

import datamass.Usuarios.Andre;
import datamass.Usuarios.Sandra;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Test;
import setup.Setup;
import utils.GeradorCPF;
import utils.GerarDados;

import java.util.HashMap;
import java.util.Map;

public class AlterarParcelas extends Setup {

    @Test
    public void alterarParcelasComSucesso(){

        Map<Object, Object> params = new HashMap<>();
        GerarDados gerar = new GerarDados();
        gerar.massaAlterarParcela();

        params.put("nome", Sandra.nome);
        params.put("cpf", Sandra.cpf);
        params.put("email",Sandra.email);
        params.put("valor",Sandra.valor);
        params.put("parcelas","10");
        params.put("seguro",Sandra.seguro);

        RestAssured.given()
                .contentType("application/json")
                .body(params)
                .when()
                .put("/simulacoes/"+Sandra.cpf)
                .then()
                .statusCode(200)
                .body("id", Matchers.is(Matchers.notNullValue()))
                .body("parcelas", Matchers.is(10));
    }

}
