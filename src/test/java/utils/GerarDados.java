package utils;

import com.google.gson.JsonObject;
import datamass.Usuarios.Andre;
import datamass.Usuarios.Marcos;
import datamass.Usuarios.Maria;
import datamass.Usuarios.Sandra;


import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class GerarDados {

   public Number gerarId(){

       Map<Object, Object> params = new HashMap<>();

       params.put("nome", Marcos.nome);
       params.put("cpf", Marcos.cpf);
       params.put("email", Marcos.email);
       params.put("valor", Marcos.valor);
       params.put("parcelas", Marcos.parcelas);
       params.put("seguro", Marcos.seguro);

        Number id = given().contentType("application/json").body(params).when().post("/simulacoes").then().statusCode(201).extract().body().as(JsonObject.class).get("id").getAsNumber();

        System.out.println("id da simulacao "+id);
         return id;


    }

    public String cpfCadastrado(){

        Map<Object, Object> params = new HashMap<>();

        params.put("nome", Marcos.nome);
        params.put("cpf", Marcos.cpf);
        params.put("email", Marcos.email);
        params.put("valor", Marcos.valor);
        params.put("parcelas", Marcos.parcelas);
        params.put("seguro", Marcos.seguro);

        String cpf  = given().contentType("application/json").body(params).when().post("/simulacoes").then().statusCode(201).extract().body().as(JsonObject.class).get("cpf").getAsString();

        System.out.println("cpf da simulacao "+cpf);
        return cpf;

    }

    public Number massadelete(){

        Map<Object, Object> params = new HashMap<>();

        params.put("nome", Maria.nome);
        params.put("cpf", Maria.cpf);
        params.put("email", Maria.email);
        params.put("valor", Maria.valor);
        params.put("parcelas", Maria.parcelas);
        params.put("seguro", Maria.seguro);

        Number id = given().contentType("application/json").body(params).when().post("/simulacoes").then().statusCode(201).extract().body().as(JsonObject.class).get("id").getAsNumber();

        System.out.println("id da simulacao "+id);
        return id;

    }

    public String massaAlterarSimulacao(){

        Map<Object, Object> params = new HashMap<>();

        params.put("nome", Andre.nome);
        params.put("cpf", Andre.cpf);
        params.put("email", Andre.email);
        params.put("valor", Andre.valor);
        params.put("parcelas", Andre.parcelas);
        params.put("seguro", Andre.seguro);

        String cpf  = given().contentType("application/json").body(params).when().post("/simulacoes").then().statusCode(201).extract().body().as(JsonObject.class).get("cpf").getAsString();

        System.out.println("cpf da simulacao "+cpf);
        return cpf;

    }


    public String massaAlterarParcela(){

        Map<Object, Object> params = new HashMap<>();

        params.put("nome", Sandra.nome);
        params.put("cpf", Sandra.cpf);
        params.put("email", Sandra.email);
        params.put("valor", Sandra.valor);
        params.put("parcelas", Sandra.parcelas);
        params.put("seguro", Sandra.seguro);

        String cpf  = given().contentType("application/json").body(params).when().post("/simulacoes").then().statusCode(201).extract().body().as(JsonObject.class).get("cpf").getAsString();

        System.out.println("cpf da simulacao "+cpf);
        return cpf;

    }




}
