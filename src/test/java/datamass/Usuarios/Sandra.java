package datamass.Usuarios;

import utils.GeradorCPF;

public class Sandra {

    static GeradorCPF gerarcpf = new GeradorCPF();

    public static final String nome = "sandra silva";
    public static final String cpf = gerarcpf.geraCPF();
    public static final String email = "sandra@email.com";
    public static final int valor = 3000;
    public static final int parcelas = 3;
    public static final boolean seguro= true;
}
