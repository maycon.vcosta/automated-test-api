package datamass.Usuarios;

import utils.GeradorCPF;

public class Andre {

    static GeradorCPF gerarcpf = new GeradorCPF();

    public static final String nome = "Andre Filipe";
    public static final String cpf = gerarcpf.geraCPF();
    public static final String email = "andre@email.com";
    public static final int valor = 3000;
    public static final int parcelas = 8;
    public static final boolean seguro= true;
}
