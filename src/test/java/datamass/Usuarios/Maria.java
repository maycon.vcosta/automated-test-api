package datamass.Usuarios;

import utils.GeradorCPF;

public class Maria {

    static GeradorCPF gerarcpf = new GeradorCPF();

    public static final String nome = "Maria Morais";
    public static final String cpf = gerarcpf.geraCPF();
    public static final String email = "maria@email.com";
    public static final int valor = 2000;
    public static final int parcelas = 5;
    public static final boolean seguro= true;
}
