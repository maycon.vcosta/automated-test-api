package datamass.Usuarios;

import utils.GeradorCPF;

public class Marcos {

    static GeradorCPF gerarcpf = new GeradorCPF();

    public static final String nome = "Marcos Alonso";
    public static final String cpf = gerarcpf.geraCPF();
    public static final String email = "Marcos@email.com";
    public static final int valor = 2000;
    public static final int parcelas = 10;
    public static final boolean seguro= true;
}
