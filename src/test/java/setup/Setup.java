package setup;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import org.junit.BeforeClass;

import java.util.HashMap;
import java.util.Map;

public class Setup {

    @BeforeClass
    public static void setup(){

    RestAssured.baseURI ="http://localhost:8080/api/v1";

    RequestSpecBuilder requestBuilder = new RequestSpecBuilder();
    requestBuilder.log(LogDetail.ALL);
    RestAssured.requestSpecification = requestBuilder.build();
    ResponseSpecBuilder responseBuilder = new ResponseSpecBuilder();
    responseBuilder.log(LogDetail.ALL);
    RestAssured.responseSpecification = responseBuilder.build();
    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL);




    }

}
