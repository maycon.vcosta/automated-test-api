package runner;

import org.junit.runner.RunWith;
import org.junit.runners.*;
import setup.Setup;
import tasks.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ConsultarRestricoes.class,
        CriarSimulacoes.class,
        ConsultarSimulacao.class,
        DeletarSimulacao.class,
        AlterarParcelas.class,
        AlterarNome.class

})
public class SuiteTest extends Setup {

}
