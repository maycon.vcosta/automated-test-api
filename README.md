# Automated-Test-API

O objetivo desse framework é facilitar o desenvolvimento de testes automatizados de API tranzendo boas práticas de desenvolvimento que ajudam o time na hora implementar testes, foi  utilizado ferramentas como Rest-assured, Junit e linguagem de programaçao java 8

## Como executar a suite de testes

A classe Suite de testes contem todas as classes de testes contendo um total de 11 ct's implementados ostestes podem ser executados através do comando maven:

```
mvn test

```

```java

package runner;

import org.junit.runner.RunWith;
import org.junit.runners.*;
import setup.Setup;
import tasks.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ConsultarRestricoes.class,
        CriarSimulacoes.class,
        ConsultarSimulacao.class,
        DeletarSimulacao.class,
        AlterarParcelas.class,
        AlterarNome.class

})
public class SuiteTest extends Setup {

}

```


## Gerenciamento de massa

# Gerador de CPF
O gerenciamento de massa é feito pela classes GerarCPF que gera numeros de CPF validos para testes na aplicação.


# Gerar dados

Essa classe é responsavel por gerar massas consistentes que pode ser utlizadas nos cenários de testes como por exemplo cadastrar simulações de clientes que podem ser utilizados como pre-condição para outros cenários.

``` java

  public String cpfCadastrado(){

        Map<Object, Object> params = new HashMap<>();

        params.put("nome", Marcos.nome);
        params.put("cpf", Marcos.cpf);
        params.put("email", Marcos.email);
        params.put("valor", Marcos.valor);
        params.put("parcelas", Marcos.parcelas);
        params.put("seguro", Marcos.seguro);

        String cpf  = given().contentType("application/json")
        .body(params)
        .when()
        .post("/simulacoes")
        .then()
        .statusCode(201)
        .extract().body().as(JsonObject.class).get("cpf").getAsString();

        System.out.println("cpf da simulacao "+cpf);
        return cpf;

    }

```

# Usuarios

A classe usuario é responsavel por armazenar informações que são requeridas para os requests de simulações.

```java
package datamass.Usuarios;


import utils.GeradorCPF;

public  class Davi {

   static GeradorCPF gerarcpf = new GeradorCPF();

            public static final String nome = "Davi Campos";
            public static final String cpf = gerarcpf.geraCPF();
            public static final String email = "Davi@email.com";
            public static final int valor = 1200;
            public static final int parcelas = 3;
            public static final boolean seguro= true;

    }

    ```

